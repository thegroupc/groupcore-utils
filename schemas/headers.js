const headers = {
  type: "object",
  properties: {
    token: {
      type: "string",
    },
  },
  required: ["token"],
};

module.exports = headers;
