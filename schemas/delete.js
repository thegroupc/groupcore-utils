const headers = require("./headers");

const schema = {
  headers,
  querystring: {
    type: "object",
    maxProperties: 1,
    properties: {
      id: { type: "number" },
      email: { type: "string" },
    },
    oneOf: [
      {
        required: ["id"],
      },
      {
        required: ["email"],
      },
    ],
  },

  response: {
    204: {
      type: "null",
    },
  },
};

module.exports = schema;
