const EasyRbac = require('easy-rbac');

module.exports = class {
  /**
   * @method validateAction()
   * @description validate the action to be carried out
   * @param {number} id - the main id
   * @param {number} compareId - id to be compared
   * @returns {Promise<void>}
   */
  static async validateAction(id, compareId) {
    const rbac = new EasyRbac({
      user: {
        can: [
          {
            name: 'do',
            when: async (params) => params.id === params.compareId,
          },
        ],
      },
    });

    const response = await rbac.can('user', 'do', {
      id,
      compareId,
    });

    if (!response) {
      throw new Error('User not authorized to perform this action!');
    }
  }
};
