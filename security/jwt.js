const jwt = require('jsonwebtoken');

module.exports = class {
  /**
   * @description constructor
   * @param {string} key - jwt key
   */
  constructor(key) {
    this.key = key;
  }

  /**
   * @method jwtSign()
   * @description create a JWT token
   * @param {object} payload - payload to be tokenized
   * @returns {*}
   */
  sign(payload) {
    return jwt.sign(payload, this.key);
  }

  /**
   * @method jwtDecode()
   * @description decode a JWT token
   * @param {string} token - JWT token to be decoded
   * @returns {{payload: any, signature: *, header: *}}
   */
  static decode(token) {
    return jwt.decode(token);
  }

  /**
   * @method jwtVerify()
   * @description verify a JWT token
   * @param {string} token - token to be verified
   * @returns {*}
   */
  verify(token) {
    return jwt.verify(token, this.key);
  }
};
