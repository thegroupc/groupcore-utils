const bcrypt = require('bcrypt');

module.exports = class {
  /**
   * @description constructor
   */
  constructor() {
    this.saltRounds = 10;
  }

  /**
   * @method hash()
   * @description create a hash of a text
   * @param {string} text - text to be hashed
   * @returns {*}
   */
  hash(text) {
    return bcrypt.hashSync(text, bcrypt.genSaltSync(this.saltRounds));
  }

  /**
   * @method verify()
   * @description verify a text by its hash
   * @param {string} hash - the hashed text
   * @param {string} text - the text to be compared with the hash
   * @returns {*}
   */
  static verify(hash, text) {
    return bcrypt.compareSync(text, hash);
  }
};
