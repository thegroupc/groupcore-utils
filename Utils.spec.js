const Utils = require('./Utils');
const util = new Utils({ projectId: 10, apiToken: 'sampletoken', apiUrl: 'https://' });
const axios = require('axios');

jest.mock('axios');

describe('Testing Utils.submitForm()', () => {
  it('Should get a status 204 as response', async () => {
    axios.mockResolvedValue({ status: 200 });
    const response = await util.submitForm({ formData: { name: 'abcd' }, formId: 300, email: '1234' });
    expect(response.status).toEqual(200);
  });

  it('Should return error if missing params', async () => {
    try {
      await util.submitForm({ formId: 1234, email: '1234', leadId: 1234 });
    } catch (error) {
      expect(error).toEqual({ error: 'Missing some required params' });
    }
  });

  it('Should catch any errors during API call', async () => {
    axios.mockRejectedValue();

    try {
      await util.submitForm({ formData: { name: 'abcd' }, formId: 1234, email: '1234' });
    } catch (error) {
      expect(error).toEqual({ error: 'API error' });
    }
  });
});

describe('Testing Utils.sendMail()', () => {
  it('It should return right response', async () => {
    axios.mockResolvedValue({ status: 204 });
    const response = await util.sendMail({
      recipientEmail: '1234',
      subject: 'abcd',
      htmlBody: '<p>abcd</p>',
    });
    expect(response.status).toEqual(204);
  });

  it('It should return right response with attachment', async () => {
    axios.mockResolvedValue({ status: 204 });
    const response = await util.sendMail({
      recipientEmail: 'abcd',
      subject: 'abcd',
      htmlBody: '<p>abcd</p>',
      hasAttachment: true,
      attachmentFilename: 'abcd',
      attachmentUrl: 'abcd',
    });
    expect(response.status).toEqual(204);
  });

  it('Should require attachmentFilename and attachmentUrl if hasAttachment is true', async () => {
    try {
      await util.sendMail({
        recipientEmail: 'abcd',
        subject: 'abcd',
        htmlBody: '<p>abcd</p>',
        hasAttachment: true,
        attachmentFilename: 'abcd',
      });
    } catch (error) {
      expect(error).toEqual({ error: 'Some missing params. If hasAttachment is true, both attachmentFilename and attachmentUrl are required' });
    }
  });

  it('Should return error if hasAttachment is false or undefined but attachmentUrl or attachmentFilename are not empty', async () => {
    try {
      await util.sendMail({
        recipientEmail: 'abcd',
        subject: 'abcd',
        htmlBody: '<p>abcd</p>',
        attachmentFilename: 'abcd',
      });
    } catch (error) {
      expect(error).toEqual({ error: 'If hasAttachment is false, both attachmentFilename and attachmentUrl cannot be accepted' });
    }
  });

  it('Should return error if missing required params', async () => {
    try {
      await util.sendMail({
        recipientEmail: '1234',
      });
    } catch (error) {
      expect(error).toEqual({ error: 'Missing some required params' });
    }
  });

  it('Should catch any errors during API call', async () => {
    axios.mockRejectedValue();

    try {
      await util.sendMail({
        recipientEmail: '1234',
        subject: 'abcd',
        htmlBody: '<p>abcd</p>',
      });
    } catch (error) {
      expect(error).toEqual({ error: 'API error' });
    }
  });
});

describe('Testing Utils.addLead()', () => {
  it('It should return right response', async () => {
    axios.mockResolvedValue({ data: { insertId: 3 } });
    const response = await util.addLead({ firstname: 'test', lastname: 'test', email: 'abcd', phone: '123456' });
    expect(response.data.insertId).toEqual(3);
  });

  it('Should return error if missing required params', async () => {
    try {
      await util.addLead({
        firstname: '1234',
      });
    } catch (error) {
      expect(error).toEqual({ error: 'Missing some required params' });
    }
  });

  it('Should catch any errors during API call', async () => {
    axios.mockRejectedValue();

    try {
      await util.addLead({ firstname: 'test', lastname: 'test', email: 'abcd', phone: '123456' });
    } catch (error) {
      expect(error).toEqual({ error: 'API error' });
    }
  });
});

describe('Testing Utils.getGroupInventory()', () => {
  it('Should return the right response', async () => {
    axios.mockResolvedValue({
      data: [
        {
          id: 13,
          item: '{"item":"abcd","price":12.34}',
          inventoryGroup: 6,
          status: 0,
          dateAdded: 0,
          cost: 8,
          data: {
            test: 'abcd',
          },
        },
      ],
    });

    const response = await util.getGroupInventory({ groupId: 6 });
    expect(response.id).toEqual(13);
  });

  it('Should validate the group ID param', async () => {
    try {
      await util.getGroupInventory({ groups: 6 });
    } catch (error) {
      expect(error).toEqual({ error: 'Missing some required params' });
    }
  });

  it('Should catch any errors during API call', async () => {
    axios.mockRejectedValue();
    try {
      await util.getGroupInventory({ groupId: 6 });
    } catch (error) {
      expect(error).toEqual({ error: 'API error' });
    }
  });
});

describe('Testing Utils.getSectionContent()', () => {
  it('Should return the right response', async () => {
    axios.mockResolvedValue({
      data: {
        testLabel: 'this is a test label',
      },
    });

    const response = await util.getSectionContent({ groupId: 6 });
    expect(response.testLabel).toEqual('this is a test label');
  });

  it('Should validate the group ID param', async () => {
    try {
      await util.getSectionContent({ groups: 6 });
    } catch (error) {
      expect(error).toEqual({ error: 'Missing some required params' });
    }
  });

  it('Should catch any errors during API call', async () => {
    axios.mockRejectedValue();

    try {
      await util.getSectionContent({ groupId: 6 });
    } catch (error) {
      expect(error).toEqual({ error: 'API error' });
    }
  });
});
