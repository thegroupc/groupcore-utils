module.exports = class {
  setQuery(query) {
    this.query = query;
  }

  setBody(body) {
    this.body = body;
  }

  setHeaders(headers) {
    this.headers = headers;
  }

  setToken(token) {
    this.token = token;
  }
};
