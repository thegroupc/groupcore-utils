module.exports = class {
  constructor() {
    jest.spyOn(this, 'send').mockImplementation((body) => body);
    jest.spyOn(this, 'status').mockImplementation(() => this);
  }

  static send() {
    return true;
  }

  static status() {
    return true;
  }
};
