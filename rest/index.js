const axios = require('axios');

module.exports = {
  /**
   * @method get()
   * @description send a GET request
   * @param {string} url - api url
   * @param {object} headers - headers to be added to request
   * @returns {Promise<any>}
   */
  async get(url, headers = {}) {
    return axios({
      url,
      headers,
      method: 'get',
    });
  },

  /**
   * @method post()
   * @description send a POST request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request
   * @returns {Promise<any>}
   */
  async post(url, data, headers = {}) {
    return axios({
      url,
      data,
      headers,
      method: 'post',
    });
  },

  /**
   * @method put()
   * @description send PUT request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request
   * @returns {Promise<any>}
   */
  async put(url, data, headers = {}) {
    return axios({
      url,
      data,
      headers,
      method: 'put',
    });
  },

  /**
   * @method patch()
   * @description send PATCH request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request
   * @returns {Promise<any>}
   */
  async patch(url, data, headers = {}) {
    return axios({
      url,
      data,
      headers,
      method: 'patch',
    });
  },

  /**
   * @method delete()
   * @description send DELETE request
   * @param {string} url - api url
   * @param {object} headers - headers to be added to request
   * @returns {Promise<any>}
   */
  async delete(url, headers = {}) {
    return axios({
      url,
      headers,
      method: 'delete',
    });
  },
};
