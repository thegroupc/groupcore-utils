## Group Core Util Package
### Description
This package provides methods to work with parts of your Core account to enable you build solutions around the Core features.

### Installation
```
npm install groupcore-utils
```

### Basic Usage
```
import CoreUtils from 'groupcore-utils'

...
const projectId = 'your Core project ID'
const apiToken = 'your Core project API token'
const apiUrl = 'your Core API URL'

const coreUtil = new CoreUtils({ projectId, apiToken, apiUrl })

// now you can call any of the available methods
// for example, to get the inventory items of a particular inveentory group with ID of 1
coreUtil.getGroupInventory({groupId: 1}).then((response)=>{
    ....
})

```

### Available Methods
These are the methods you can call with this package. This list will continually be updated as needs arise.

#### submitForm()
```
  /**
   * Submit a form to the Core Lead Management module
   * @param {object} formData - Key-value pair of all form fields to be submitted
   * @param {number} formId - The ID of the Core form for the submission
   * @param {string} email - The email address of the submitter
   * @param {number} [leadId=null] - The lead ID, if the addLead method is called before submission
   * @returns {Promise}
   */
```


#### sendMail()
```
  /**
   * Send an email message from your Core account.
   * @param {string} recipientEmail - Email address of recipient
   * @param {string} subject - Subject of the email
   * @param {string} htmlBody - HTML body of the email message
   * @param {boolean} [hasAttachment] - Specify if an attachment will be sent with the email
   * @param {string} [attachmentFilename] - Choose the filename of the attachment. Optional if hasAttachment is false
   * @param {string} [attachmentUrl] - The location of the attachment. Optional if hasAttachment is false
   * @returns {Promise}
   */
```

#### addLead()
```
  /**
   * Add a lead to the Core Lead Management module
   * @param {string} firstname - The first name of the lead
   * @param {string} lastname - The last name of the lead
   * @param {string} phone - The phone number of the lead
   * @param {string} email - The email address of the lead
   * @returns {Promise}
   */
```

#### getGroupInventory()
```
  /**
   * get inventory by group
   * @param {number} groupId - The ID of the Core inventory group
   * @returns {Promise}
   */
```

#### getSectionContent()
```
  /**
   * Get content from the Core content management system, by section
   * @param {number} groupId - The ID of the Core inventory group
   * @returns {Promise}
   */
```

### Communication
#### Email
Send mail to a specified recipient.
```
  require('groupcore-utils/communication/email')
    
  /**
   * @description constructor
   * @param {string} hostname - hostname of the mail service provider
   * @param port {number} - smtp port of the mail service provider
   * @param username {string} - smtp username
   * @param password {password} - smtp password
   */

  /**
   * @method send()
   * @description send email to a specified recipient
   * @param email {string} - the recipient's email address
   * @param subject {string} - subject of the email
   * @param message {string} - mail message
   * @param attachment {string} - url of attached file
   * @param from {string} - sender email address
   * @returns {Promise<*>}
   */
```

### SQL Database
#### Initialization
Initialize db with this and pass the object to the CRUD class constructor
```
  require('groupcore-utils/database/init')
  
   /**
   * @description constructor
   * @param {string} host - sql db hostname
   * @param {string} user - db user username
   * @param {string} password - db user password
   * @param {string} db - db name
   * @param {string} socket - path to a unix domain socket to connect to. when used host and port are ignored
   */
```

#### CRUD Methods
```
   require('groupcore-utils/database/crud')

  /**
   * @description constructor
   * @param {string} dbTable - name of the table
   * @param {object} db - the initialised db instance
   */
  
  /**
   * @method create()
   * @description Insert values into db
   * @param {Object} data - the key value pair of the data to be inserted into the db
   * @param {Boolean} unique - if set to true, ensure no duplication of data during creation
   * @param {Array} ignore - fields to ignore when checking for unique values
   * @returns {Promise | string | Object} - resolves if successfully and rejects if there's an error
   */
   
  /**
   * @method read()
   * @description get data from table
   * @param {null, Object} where - if no where, all the data will be returned from db table without any 'where' clause.  shape {field, value}
   * @param {null | Array} orderBy - explicitly specify the order, either asc or desc
   * @returns {Promise}
   */
   
  /**
   * @method update
   * @description update db table by id
   * @param {Object} data - fields to update
   * @param {number} id - id in where clause
   * @returns {Promise}
   */  
   
  /**
   * @method delete()
   * @description delete data from db by id
   * @param {number} id - id of data to deleted
   * @returns {Promise}
   */
   
  /**
   * @method query()
   * @description make a custom db query
   * @param {string} queryStatement - SQL query
   * @returns {Promise}
   */
```

### REST
```
  require('groupcore-utils/rbac')

  /**
   * @method get()
   * @description send a GET request
   * @param {string} url - api url
   * @param {object} headers - headers to be added to request
   * @returns {AxiosPromise<any>}
   */
   
  /**
   * @method post()
   * @description send a POST request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request 
   * @returns {AxiosPromise<any>}
   */
   
  /**
   * @method put()
   * @description send PUT request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request
   * @returns {AxiosPromise<any>}
   */
   
  /**
   * @method patch()
   * @description send PATCH request
   * @param {string} url - api url
   * @param {object} data - payload
   * @param {object} headers - headers to be added to request
   * @returns {AxiosPromise<any>}
   */
   
  /**
   * @method delete()
   * @description send DELETE request
   * @param {string} urls - api url
   * @param {object} headers - headers to be added to request
   * @returns {AxiosPromise<any>}
   */
```

### Encryption
```
  require('groupcore-utils/security/encryption')
  /**
   * @method hash()
   * @description create a hash of a text
   * @param {string} text - text to be hashed
   * @returns {*}
   */
   
  /**
   * @method verify()
   * @description verify a text by its hash
   * @param {string} hash - the hashed text
   * @param {string} text - the text to be compared with the hash
   * @returns {*}
   */  
```

### JWT
```
  require('groupcore-utils/security/encryption')

  /**
   * @description constructor
   * @param {string} jwtKey - jwt key
   */

  /**
   * @method jwtSign()
   * @description create a JWT token 
   * @param {object} payload - payload to be tokenized
   * @returns {*}
   */

  /**
   * @method jwtDecode()
   * @description decode a JWT token
   * @param {string} token - JWT token to be decoded
   * @returns {{payload: any, signature: *, header: *}|*|SourceMapPayload|TokenPayload}
   */      
   
  /**
   * @method jwtVerify()
   * @description verify a JWT token
   * @param {string} token - token to be verified
   * @returns {*}
   */         
```

### Date and Time
```
  require('groupcore-utils/timestamp')

  /**
   * @method now()
   * @description returns the present unix timestamp
   * @returns {number}
   */
   
  /**
   * @method unixToDate()
   * @description convert a unix timestamp to specific date format
   * @param {number} unix - the unix timestamp to be converted
   * @param {string} format - the format to use, default is 'MMMM DD, YYYY'
   * @returns {string}
   */
   
  /**
   * @method dateToUnix()
   * @description convert a unix date to unix
   * @param {number} date - the date to be converted
   * @param {string} format - the format of the date
   * @returns {number}
   */   
   
  /**
   * @method unixToStartOfDay()
   * @description take whatever date is given to 12:00:00 AM of that day
   * @param {string} unix - unix timestamp
   * @returns {number}
   */   
   
  /**
   * @method unixToEndOfDay()
   * @description take whatever date is given to 11:59:59 PM of the day
   * @param {string} unix - unix timestamp
   * @returns {number}
   */   
   
  /**
   * @method toSeconds()
   * @description convert time (HH:MM) to seconds
   * @param {string} time - time to be converted
   * @returns {number}
   */   
```

### RBAC
```
  require('@groupcollab/core-utils/rbac')

  /**
   * @method validateAction()
   * @description validate the action to be carried out
   * @param {number} id - the main id
   * @param {number} compareId - id to be compared
   * @returns {Promise<void>}
   */
```

### Testing
```
npm run test
```