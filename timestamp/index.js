const moment = require('moment');

module.exports = class {
  /**
   * @method now()
   * @description returns the present unix timestamp
   * @returns {number}
   */
  static now() {
    return moment().unix();
  }

  /**
   * @method unixToDate()
   * @description convert a unix timestamp to specific date format
   * @param {number} unix - the unix timestamp to be converted
   * @param {string} format - the format to use, default is 'MMMM DD, YYYY'
   * @returns {string}
   */
  static unixToDate(unix, format) {
    return moment.unix(unix).format(format);
  }

  /**
   * @method dateToUnix()
   * @description convert a unix date to unix
   * @param {number} date - the date to be converted
   * @param {string} format - the format of the date
   * @returns {number}
   */
  static dateToUnix(date, format) {
    return moment(date, format).valueOf() / 1000;
  }

  /**
   * @method unixToStartOfDay()
   * @description take whatever date is given to 12:00:00 AM of that day
   * @param {string} unix - unix timestamp
   * @returns {number}
   */
  static unixToStartOfDay(unix) {
    return new Date(unix * 1000).setHours(0, 0, 0, 0) / 1000;
  }

  /**
   * @method unixToEndOfDay()
   * @description take whatever date is given to 11:59:59 PM of the day
   * @param {string} unix - unix timestamp
   * @returns {number}
   */
  static unixToEndOfDay(unix) {
    return new Date(unix * 1000).setHours(23, 59, 59, 999) / 1000;
  }

  static unixToStartOfMonth(unix) {}

  static unixToEndOfMonth(unix) {}

  /**
   * @method toSeconds()
   * @description convert time (HH:MM) to seconds
   * @param {string} time - time to be converted
   * @returns {number}
   */
  static toSeconds(time) {
    const splitTime = time.split(':');
    return splitTime[0] * 3600 + splitTime[1] * 60;
  }
};
