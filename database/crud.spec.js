const mysql = require('mysql');
const Crud = require('./crud');
const Init = require('./init');

jest.spyOn(mysql, 'createConnection').mockImplementation(() => {
  console.log('connected');
});

const db = new Init({ host: '', user: '', password: '', db: '' });
describe('read()', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should work when "where" is an object', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({ where: { field: 'id', value: '10' }, orderBy: null });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10'");
    expect(querySpy).toHaveBeenCalledTimes(1);
  });

  it('should work when "where" is an array of objects with "and" condition', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: {
        conditions: ['and'],
        fields: [
          { field: 'id', value: '10' },
          { field: 'username', value: 'test' },
        ],
      },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10' and username = 'test'");
  });

  it('should work when "where" is an array of objects with "or" condition', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: {
        conditions: ['or'],
        fields: [
          { field: 'id', value: '10' },
          { field: 'tag', value: 'test' },
        ],
      },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10' or tag = 'test'");
  });

  it('should work when "where" has just one in array', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: {
        conditions: null,
        fields: [{ field: 'id', value: '10' }],
      },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10'");
  });

  it('should work when "where" is an array of objects with multiple conditions', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: {
        conditions: ['or', 'and'],
        fields: [
          { field: 'id', value: '10' },
          { field: 'tag', value: 'test' },
          { field: 'slug', value: 'testslug' },
        ],
      },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10' or tag = 'test' and slug = 'testslug'");
  });

  it('should work when "where" is an array of objects with one condition', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: {
        condition: 'and',
        fields: [
          { field: 'id', value: '10' },
          { field: 'tag', value: 'test' },
          { field: 'slug', value: 'testslug' },
        ],
      },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = '10' and tag = 'test' and slug = 'testslug'");
  });

  it('should work without any conditions', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: null,
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith('select * from test');
  });

  it('should support deprecated usage', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });
    await new Crud({ dbTable: 'test', db }).read({
      where: { field: 'id', value: 'test' },
      orderBy: null,
    });
    expect(querySpy).toHaveBeenCalledWith("select * from test where id = 'test'");
  });
});

describe('update()', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should generate the right query', async () => {
    expect.hasAssertions();

    const querySpy = jest.spyOn(Init.prototype, 'query').mockImplementation(() => {
      console.log('query called');
    });

    await new Crud({ dbTable: 'test', db }).update({ data: { id: 1, tag: 'test', slug: 'test' }, id: 1 });
    expect(querySpy).toHaveBeenCalledWith("update test set tag = 'test', slug = 'test' where id = '1'");
    expect(querySpy).toHaveBeenCalledTimes(1);
  });
});
