const escapeQuotes = require('escape-quotes');
const _ = require('lodash');

module.exports = class {
  /**
   * @description constructor
   * @param {string} dbTable - name of the table
   * @param {object} db - the initialised db instance
   */
  constructor(dbTable, db) {
    this.dbTable = dbTable;
    this.db = db;
  }

  /**
   * @method create()
   * @description Insert values into db
   * @param {Object} data - the key value pair of the data to be inserted into the db
   * @returns {Promise | string | Object} - resolves if successfully and rejects if there's an error
   */

  async create(data) {
    const fields = Object.keys(data).join(',');

    const rawValues = [];
    Object.values(data).map((value) => {
      rawValues.push(`'${escapeQuotes(value)}'`);
      return true;
    });
    const values = rawValues.join(',');

    const query = `insert into ${this.dbTable} (${fields}) values (${values})`;

    return this.db.query(query);
  }

  /**
   * @method read()
   * @description get data from table
   * @param {Object | null} [where] - if no where, all the data will be returned from db table without any 'where' clause.
   * shape {fields (array of fields {field, value}), condition array ("and" or "or") to correspond with fields}
   * @param {Array | null} [orderBy] - explicitly specify the order, either asc or desc
   * @returns {Promise}
   */
  read(where, orderBy) {
    if (where && !_.has(where, 'fields')) {
      return this.readV1(where, orderBy);
    }

    let query;
    let whereClause = '';
    if (where) {
      where.fields.forEach((field, i) => {
        if (_.last(where.fields) === field) {
          whereClause += `${field.field} = '${field.value}'`;
        } else if (where.conditions) {
          whereClause += `${field.field} = '${field.value}' ${where.conditions[i]} `;
        } else {
          whereClause += `${field.field} = '${field.value}' ${where.condition} `;
        }
      });
    }

    if (orderBy) {
      if (where) {
        query = `select * from ${this.dbTable} where ${whereClause} order by ${orderBy}`;
      } else {
        query = `select * from ${this.dbTable} order by ${orderBy}`;
      }
    } else if (where) {
      query = `select * from ${this.dbTable} where ${whereClause}`;
    } else {
      query = `select * from ${this.dbTable}`;
    }

    return this.db.query(query);
  }

  // should be deprecated soon
  readV1(where = false, orderBy = false) {
    let query;

    if (orderBy) {
      if (where) {
        query = `select * from ${this.dbTable} where ${where.field} = '${where.value}' order by ${orderBy}`;
      } else {
        query = `select * from ${this.dbTable} order by ${orderBy}`;
      }
    } else if (where) {
      query = `select * from ${this.dbTable} where ${where.field} = '${where.value}'`;
    } else {
      query = `select * from ${this.dbTable}`;
    }

    return this.db.query(query);
  }

  /**
   * @method update
   * @description update db table by id
   * @param {Object} data - fields to update
   * @param {number} id - id in where clause
   * @returns {Promise}
   */
  update(data, id) {
    // variable to hold the dynamic part of the update sql query
    let updateQuery = '';

    // set this to be used inside the map function to check when we get to the last index key so that we dont add the comma to the last key in the generated sql query
    Object.keys(data)
      .filter((key) => key !== 'id')
      .forEach((key) => {
        updateQuery += `${key} = '${escapeQuotes(data[key])}', `;
        return true;
      });

    const query = `update ${this.dbTable} set ${updateQuery} where id = '${id}'`;

    return this.db.query(query.replace(',  where', ' where')); // using replace to remove the last comma before the where clause
  }

  /**
   * @method delete()
   * @description delete data from db by id
   * @param {number} id - id of data to deleted
   * @returns {Promise}
   */
  delete(id) {
    return this.db.query(`delete from ${this.dbTable} where id = '${id}'`);
  }

  /**
   * @method query()
   * @description make a custom db query
   * @param {string} query - SQL query
   * @returns {Promise}
   */
  // eslint-disable-next-line class-methods-use-this
  query(query) {
    return this.db.query(query);
  }
};
