const mysql = require('mysql');

module.exports = class {
  /**
   * @description constructor
   * @param {string | null} host - sql db hostname
   * @param {string} user - db user username
   * @param {string} password - db user password
   * @param {string} db - db name
   * @param {string | null} socket - path to a unix domain socket to connect to. when used host and port are ignored
   */
  constructor(host, user, password, db, socket) {
    this.host = host;
    this.user = user;
    this.password = password;
    this.db = db;
    this.socket = socket;
  }

  /**
   * @method query()
   * @description send a db query
   * @param {string} statement sql statement
   * @returns {Promise<unknown>}
   */
  query(statement) {
    const db = mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.db,
      multipleStatements: true,
      socketPath: this.socket,
    });

    return new Promise((resolve, reject) => {
      db.query(statement, (err, result) => {
        if (err) {
          reject(Error(`DB Error: ${err}, Statement: ${statement}`));
        }

        resolve(result);
        db.end();
      });
    });
  }
};
