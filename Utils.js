const axios = require('axios');
const { isEmpty, isUndefined } = require('lodash');

class Utils {
  /**
   * The project-specific details when creating an instance. All details can be gotten from your Core account
   * @param {number} projectId - The Core project ID
   * @param {string} apiToken - The Core API token to authenticate and authorize access
   * @param {string} apiUrl - The Core API URL provided during Core setup
   */
  constructor(projectId, apiToken, apiUrl) {
    this.project = projectId;
    this.token = apiToken;
    this.apiUrl = apiUrl;
  }

  /**
   * Submit a form to the Core Lead Management module
   * @param {object} formData - Key-value pair of all form fields to be submitted
   * @param {number} formId - The ID of the Core form for the submission
   * @param {string} email - The email address of the submitter
   * @param {number} [leadId=null] - The lead ID, if the addLead method is called before submission
   * @returns {Promise}
   */
  submitForm(formData, formId, email, leadId = 0) {
    return new Promise((resolve, reject) => {
      if (isEmpty(formData) || isEmpty(String(formId)) || isEmpty(email)) {
        reject(new Error('Missing some required params'));
        return;
      }

      axios({
        method: 'POST',
        url: `${this.apiUrl}/com/submissions`,
        data: {
          formData,
          formId,
          email,
          leadId,
        },
        headers: {
          token: this.token,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch(() => {
          reject(new Error('API error'));
        });
    });
  }

  /**
   * Send an email message from your Core account.
   * @param {string} recipientEmail - Email address of recipient
   * @param {string} subject - Subject of the email
   * @param {string} htmlBody - HTML body of the email message
   * @param {boolean} [hasAttachment] - Specify if an attachment will be sent with the email
   * @param {string} [attachmentFilename] - Choose the filename of the attachment. Optional if hasAttachment is false
   * @param {string} [attachmentUrl] - The location of the attachment. Optional if hasAttachment is false
   * @returns {Promise}
   */
  sendMail(recipientEmail, subject, htmlBody, attachmentFilename, attachmentUrl, hasAttachment = false) {
    return new Promise((resolve, reject) => {
      if (hasAttachment) {
        if (isEmpty(attachmentFilename) || isEmpty(attachmentUrl)) {
          reject(new Error('Some missing params. If hasAttachment is true, both attachmentFilename and attachmentUrl are required'));
          return;
        }
      }

      if (!hasAttachment && (attachmentFilename || attachmentUrl)) {
        reject(new Error('If hasAttachment is false, both attachmentFilename and attachmentUrl cannot be accepted'));
        return;
      }

      if (isEmpty(recipientEmail) || isEmpty(subject) || isEmpty(htmlBody)) {
        reject(new Error('Missing some required params'));
        return;
      }

      axios({
        method: 'POST',
        url: `${this.apiUrl}/com/mail`,
        data: {
          project: this.project,
          email: recipientEmail,
          message: htmlBody,
          subject,
          attachments: hasAttachment ? [{ filename: attachmentFilename, path: attachmentUrl }] : null,
        },
        headers: {
          token: this.token,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch(() => {
          reject(new Error('API error'));
        });
    });
  }

  /**
   * Add a lead to the Core Lead Management module
   * @param {string} firstname - The first name of the lead
   * @param {string} lastname - The last name of the lead
   * @param {string} phone - The phone number of the lead
   * @param {string} email - The email address of the lead
   * @returns {Promise}
   */
  addLead(firstname, lastname, email, phone) {
    return new Promise((resolve, reject) => {
      if (isEmpty(firstname) || isEmpty(lastname) || isEmpty(email)) {
        reject(new Error('Missing some required params'));
        return;
      }

      axios({
        method: 'POST',
        url: `${this.apiUrl}/com/leads`,
        data: {
          project: this.project,
          firstname,
          lastname,
          email,
          phone: phone || null,
        },
        headers: {
          token: this.token,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch(() => {
          reject(new Error('API error'));
        });
    });
  }

  /**
   * get inventory by group
   * @param {number} groupId - The ID of the Core inventory group
   * @returns {Promise}
   */
  async getGroupInventory(groupId) {
    return new Promise((resolve, reject) => {
      if (isUndefined(groupId)) {
        reject(new Error('Missing some required params'));
        return;
      }

      axios({
        method: 'GET',
        url: `${this.apiUrl}/inv/groups?id=${groupId}`,
        headers: {
          token: this.token,
        },
      })
        .then((response) => {
          resolve(response.data[0]);
        })
        .catch(() => {
          reject(new Error('API error'));
        });
    });
  }

  /**
   * Get content from the Core content management system, by section
   * @param {number} groupId - The ID of the Core inventory group
   * @returns {Promise}
   */
  async getSectionContent(groupId) {
    return new Promise((resolve, reject) => {
      if (isUndefined(groupId)) {
        reject(new Error('Missing some required params'));
        return;
      }

      axios({
        method: 'GET',
        url: `${this.apiUrl}/cms/sections?group=${groupId}`,
        headers: {
          token: this.token,
        },
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch(() => {
          reject(new Error('API error'));
        });
    });
  }
}

module.exports = Utils;
