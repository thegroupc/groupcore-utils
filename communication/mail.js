const nodemailer = require('nodemailer');

module.exports = class {
  /**
   * @description constructor
   * @param {string} hostname - hostname of the mail service provider
   * @param port {number} - smtp port of the mail service provider
   * @param username {string} - smtp username
   * @param password {password} - smtp password
   */
  constructor(hostname, port, username, password) {
    this.hostname = hostname;
    this.port = port;
    this.username = username;
    this.password = password;
  }

  async init() {
    this.transporter = nodemailer.createTransport({
      host: this.hostname,
      port: this.port,
      secure: false,
      auth: {
        user: this.username,
        pass: this.password,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
  }

  /**
   * @method send()
   * @description send email to a specified recipient
   * @param email {string} - the recipient's email address
   * @param subject {string} - subject of the email
   * @param message {string} - mail message
   * @param attachment {string} - url of attached file
   * @param from {string} - sender email address
   * @returns {Promise<*>}
   */
  async send(email, subject, message, from, attachment) {
    await this.init();

    return this.transporter.sendMail({
      subject,
      from,
      to: email,
      html: message,
      attachments: attachment || null,
    });
  }
};
